/*Créer une fonction qui prend deux valeurs en entrée et écrit dans la console si leur type est identique ou pas, pariel pour leurs valeurs.*/

/*meme_type(2, 3); // le type est le même
meme_type(2,"2"); // le type est différent
meme_type( Math.round(), Math.round ); // le type est différent
meme_type( NaN, Infinity ); // le type est identique

meme_valeur(2, 3); // la valeur est différente
meme_valeur(2, "2"); // la valeur est identique
meme_valeur( Math.round(), Math.round ); // la valeur est différente (mais c'est un peu con)
meme_valeur( NaN, Infinity ); // la valeur est différente*/



function meme_type() {
	var test1 = "alpaga";
	var test2 = "lama";

	if (typeof test1 === typeof test2) {
		console.log("type identique");
	}

	else {
		console.log("type non identique");
	}

}

console.log(meme_type());


function meme_valeur() {
	var test3 = 6;
	var test4 = 12;

	if (test3 == test4) {
		console.log("valeur identique");
	}

	else {
		console.log("valeur non identique");
	}
}

console.log(meme_valeur());
