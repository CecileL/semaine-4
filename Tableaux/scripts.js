var tab0 = ["X", "O", "O", "X", "Z", 6, null, "X"];

var tab1 = [
	["X", "O", "X"],
	["O", "O", "X"],
	["X", "O", "O"]
];

var tab2 = [
	["X" ,"O", "X"],
	["O" ,"O", "X"],
	["X" ,"O", "O", "Z"]
];

var tab3 = [
	[ ["X","R"], ["T","Z"], ["X","Y"], ["U","F"] ],
	[ ["Q","R"], ["X","Z"], ["X","Y"], ["X","F"] ],
	[ ["V","S"], ["T","X"], ["O","Y"], ["X","F"] ],
	[ ["E","X"], ["Q","Z"], ["X","Y"], ["U","F"] ]
];

/*Comptez le nombre de croix dans ces tableaux :*/

var a = 0;

/*for (var i = 0; i < tab0.length; i++) {
	if (tab0[i] === "X") {
		a = a + 1
		console.log(a);
	}
}

for (var j = 0; j < tab1.length; j++) {
	for (var k = 0; k < tab1.length; k++) {
    if (tab1[j][k] === "X") {
      a = a + 1;
      console.log(a);
    }
	}
}

for (var l = 0; l < tab2.length; l++) {
  for (var m = 0; m < tab2.length; m++){
    if (tab2[l][m] === "X"){
      a = a + 1;
      console.log(a);
    }
  }
}

for (var n = 0; n < tab3.length; n++) {
  for (var o = 0; o < tab3.length; o++) {
    for (var p = 0; p < tab3.length; p++) {
      if (tab3[n][o][p] === "X") {
        a = a + 1;
        console.log(a);
      }
    }
  }*/


/*Convertir ce programme en une fonction (qui sera appelée 4 fois)*/

function compteur() {
    a = a + 1;
    console.log(a);
}

for (var i = 0; i < tab0.length; i++) {
	if (tab0[i] === "X") {
		compteur();
	}
}

for (var j = 0; j < tab1.length; j++) {
	for (var k = 0; k < tab1.length; k++) {
    if (tab1[j][k] === "X") {
      compteur();
    }
	}
}

for (var l = 0; l < tab2.length; l++) {
  for (var m = 0; m < tab2.length; m++){
    if (tab2[l][m] === "X"){
      compteur();
    }
  }
}

for (var n = 0; n < tab3.length; n++) {
  for (var o = 0; o < tab3.length; o++) {
    for (var p = 0; p < tab3.length; p++) {
      if (tab3[n][o][p] === "X") {
        compteur();
      }
    }
  }
}