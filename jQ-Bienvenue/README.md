# JQ-Bienvenue

## Objectif
- Manipuler le DOM.
- Utiliser la librairie de JQuery


## Contexte

Apprendre à utiliser une librairy JS.


## Compétence.s concernée.s

- Développer une UI


## Critères de réussite

- Le code est indenté et lisible.
- Il n’y a pas d’erreurs dans le code.
- Le rendu est similaire à celui du modèle, sur plusieurs navigateurs et mobile.

## Livrable.s

- Lien vers le repo gitlab pour chaques projet.
- Lien vers la version en ligne.

## Réalisation attendues

**Contrainte** :

- Commencer par installer `nodejs` et `npm`.
- Une fois fait, installer les modules avec `npm install`
- Vous devez coder uniquement dans le `./js/app.js`

___

Vous devez récuperer les valeurs des champs du formulaire, en faire un joli objet et afficher le résultat dans la balise `div#render`.